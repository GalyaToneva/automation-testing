package test.cases;

import com.telerikacademy.testframework.UserActions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import pages.WEareSocialNetwork.LoginPage;

public class BaseTestSetup {

    UserActions actions = new UserActions();

    @BeforeClass
    public static void setUp() {
        UserActions.loadBrowser("home.page");
    }



   @AfterClass
    public static void tearDown() {
       UserActions.quitDriver();
   }




    public void login(){
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginUser();
    }

    public void loginTestUser(){
        LoginPage loginPage = new LoginPage(actions.getDriver());
        loginPage.loginTestUserOne();
    }
}
