package test.cases;

import org.junit.Test;
import pages.WEareSocialNetwork.PostPage;


public class LikeCommentTest extends BaseTestSetup {
    @Test
    public void likeComment(){
        login();
        PostPage postPage = new PostPage(actions.getDriver());

        postPage.likeComment();

        postPage.unlikeComment();
    }

}
