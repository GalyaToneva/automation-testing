package test.cases;

import jdk.jfr.Description;
import org.junit.Test;
import pages.WEareSocialNetwork.RegisterPage;

public class ARegisterTest extends BaseTestSetup{

    @Test
    public void register(){
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUser();

    }

    @Test
    public void registerUserWithGraterValue(){
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUserWithValueGreaterThanMax();


    }

    @Test
    @Description("Bug")
    public void registerUserWithDidgit(){
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUserWithInvalidUsernameWithDigit();


    }


    @Test
    @Description("TEAM-103")
    public void registerUser_RandomEmailTest(){
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUser_RandomStringEmail();

    }

    @Test
    @Description("TEST-118")
    public void registerUser_NotConfirmedPassword(){
        RegisterPage registerPage = new RegisterPage(actions.getDriver());
        registerPage.registerUserNotConfirmedPassword();
    }


}
