package test.cases;

import org.junit.Test;
import pages.WEareSocialNetwork.ProfileManagement;

public class ProfileManagementTest extends BaseTestSetup {
    @Test
    public void updateBirthday(){
        login();

        ProfileManagement profileManagement = new ProfileManagement(actions.getDriver());
        profileManagement.editBirthday();
    }
    @Test
    public void updateFirstLastName(){
        login();

        ProfileManagement profileManagement = new ProfileManagement(actions.getDriver());
        profileManagement.editFirstLastName();
    }
    @Test
    public void editEmail(){
        login();
        ProfileManagement profileManagement = new ProfileManagement(actions.getDriver());
        profileManagement.editEmail();
    }


}
