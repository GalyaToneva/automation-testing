package com.telerikacademy.testframework.pages;

import com.telerikacademy.testframework.UserActions;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;

import java.util.Random;

import static com.telerikacademy.testframework.Utils.LOGGER;
import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static java.lang.String.format;

public abstract class BasePage {

    protected String url;
    protected WebDriver driver;
    public UserActions actions;


    protected String postContent = "";
    protected String generatedEmail = "";
    protected String generatedUser = "";
    protected String generatedComment = "";

    protected String generatedInvalidUser ="";
    protected String generatedUsernameWithDidgit ="";
    protected String generatedRandomStringAsEmail = "";
    Random random = new Random();
    int randomCount;


    public BasePage(WebDriver driver, String urlKey) {
        String pageUrl = getConfigPropertyByKey(urlKey);
        this.driver = driver;
        this.url = pageUrl;
        actions = new UserActions();
    }


    public void navigateToPage() {
        this.driver.get(url);
    }

    public void navigateToPage(String url) {
        this.driver.get(url);
    }

    public void assertNavigatedUrl() {
        String currentUrl = driver.getCurrentUrl();
        LOGGER.info(format("Landed URL: %s", currentUrl));

        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl + ". Expected URL: " + url,
                currentUrl.contains(url));
    }

    protected void generateRandomEmail() {
        String generatedMailUsername = RandomStringUtils.randomNumeric(2,30);
        String generatedMailServer = RandomStringUtils.randomAlphabetic(2,10);
        String generatedDomain = RandomStringUtils.randomAlphabetic(2,5);
        generatedEmail = generatedMailUsername + "@" + generatedMailServer+ "." + generatedDomain;
    }

    protected void generateRandomStringAsEmail(){
        String randomStringAsEmail = RandomStringUtils.random(30,true,true);
        generatedRandomStringAsEmail = randomStringAsEmail;

    }

    protected void generateRandomUserName() {
        String generateNum = RandomStringUtils.randomAlphabetic(3);
        generatedUser = "Tsvetomir" + generateNum;
    }

    protected void generateRandomInvalidUsernameValueGraterThenMax() {
        String generateNum = RandomStringUtils.randomAlphabetic(26);
        generatedInvalidUser = "test" + generateNum;
    }
    protected void generateRandomUsernameWithDigit() {
        String generateNum = RandomStringUtils.randomNumeric(2);
        generatedUsernameWithDidgit = "test" + generateNum;
    }

    protected void generateRandomComment() {
        String generateNum = RandomStringUtils.randomAscii(3);
        generatedComment = "Nice Post" + generateNum;
    }

    protected void generateRandomDescription() {
        randomCount = random.nextInt(10);
        while (randomCount >= 0) {
            String randomStr = RandomStringUtils.randomAlphabetic(15);
            postContent = postContent.concat(randomStr);
            randomCount--;
        }
    }

}
