package pages.WEareSocialNetwork;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

import java.util.concurrent.TimeUnit;

import java.util.Random;

import static pages.WEareSocialNetwork.utls.Constants.ANSI_GREEN;
import static pages.WEareSocialNetwork.utls.Constants.ANSI_RESET;

public class HomePage extends BaseAppPage {


    public HomePage(WebDriver driver) {
        super(driver, "home.page");
    }

    public void createPost() {
        navigateToPage();
        assertNavigatedUrl();
        generateRandomDescription();

        actions.clickElement("homePage.button.addNewPost");

        actions.waitForElementClickable("postPage.dropdown.postVisibility");

        actions.typeValueInField(postContent, "postPage.descriptionField");
        actions.clickElement("postPage.button.savePost");
        actions.waitForElementPresent("postPage.list.latestPost");

        actions.assertElementAttribute("postPage.list.latestPost", "innerText", postContent);


        System.out.printf(ANSI_GREEN +"Post with description %s was created" + ANSI_RESET, postContent);

    }


}



