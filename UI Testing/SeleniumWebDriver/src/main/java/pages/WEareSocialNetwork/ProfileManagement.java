package pages.WEareSocialNetwork;

import org.openqa.selenium.WebDriver;

import static pages.WEareSocialNetwork.utls.Constants.*;

public class ProfileManagement extends BaseAppPage {


    public ProfileManagement(WebDriver driver) {
        super(driver, "home.page");


    }

    public void editBirthday() {
        navigateToPage();
        assertNavigatedUrl();
        actions.waitForElementClickable("homePage.button.personalProfile");
        actions.clickElement("homePage.button.personalProfile");

        actions.waitForElementClickable("personalProfilePage.button.editProfile");
        actions.clickElement("personalProfilePage.button.editProfile");

        actions.waitForElementClickable("personalProfile.field.editBirthday");
        actions.clickElement("personalProfile.field.editBirthday");

        actions.typeValueInField(BIRTHDAY, "personalProfile.field.editBirthday");
        actions.clickElement("personalProfile.button.updateProfile");

        actions.waitForElementClickable("personalProfile.button.profile");
        actions.clickElement("personalProfile.button.profile");

        actions.waitForElementVisible("personalProfile.description.birthday");
        actions.assertElementPresent("personalProfile.description.birthday");
        System.out.printf("Birthday is updated\n");
    }

    public void editFirstLastName() {
        navigateToPage();
        assertNavigatedUrl();

        actions.waitForElementClickable("homePage.button.personalProfile");
        actions.clickElement("homePage.button.personalProfile");

        actions.waitForElementClickable("personalProfilePage.button.editProfile");
        actions.clickElement("personalProfilePage.button.editProfile");

        actions.waitForElementClickable("personalProfile.field.editFirstName");
        actions.typeValueInField(FIRST_NAME, "personalProfile.field.editFirstName");

        actions.waitForElementClickable("personalProfile.field.editLastName");
        actions.typeValueInField(LAST_NAME, "personalProfile.field.editLastName");
        actions.waitForElementClickable("personalProfile.button.updateProfile");
        actions.clickElement("personalProfile.button.updateProfile");

        actions.clickElement("personalProfile.button.profile");
        actions.waitForElementPresent("personalProfile.description.fullName");

        actions.assertElementAttribute("personalProfile.description.fullName",
                "innerText", FIRST_NAME + " " + LAST_NAME);
        System.out.printf("User First name was updated to: %s\nLast name was updated to: %s\n",FIRST_NAME,LAST_NAME);
    }
    public void editEmail(){
        navigateToPage();
        assertNavigatedUrl();

        actions.waitForElementClickable("homePage.button.personalProfile");
        actions.clickElement("homePage.button.personalProfile");

        actions.waitForElementClickable("personalProfilePage.button.editProfile");
        actions.clickElement("personalProfilePage.button.editProfile");

        actions.waitForElementClickable("personalProfile.field.editEmail");
        actions.typeValueInField(EMAIL,"personalProfile.field.editEmail");
        actions.clickElement("personalProfile.button.updateProfile");

        actions.clickElement("personalProfile.button.profile");
        actions.waitForElementClickable("personalProfile.description.email");

        actions.assertElementAttribute("personalProfile.description.email",
                "innerText", EMAIL);
        System.out.printf("Email was updated to:%s ",EMAIL);
    }

}
