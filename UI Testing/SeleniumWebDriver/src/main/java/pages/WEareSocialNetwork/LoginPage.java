package pages.WEareSocialNetwork;

import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;

public class LoginPage extends BaseAppPage {

    public LoginPage(WebDriver driver) {
        super(driver, "home.page");
    }



    public void loginUser() {
        String username = getConfigPropertyByKey("username");
        String password = getConfigPropertyByKey("password");


        navigateToPage();

        actions.clickElement("login.button");
        actions.waitForElementClickable("login.button.submit");

        actions.typeValueInField(username, "login.username");
        actions.typeValueInField(password, "login.password");
        actions.clickElement("login.button.submit");

        actions.waitForElementClickable("homePage.button.logout");
        actions.assertElementPresent("homePage.button.logout");

    }

    public void loginTestUserOne() {
        navigateToPage();

        String password = getConfigPropertyByKey("testUser1.password");
        actions.waitForElementClickable("login.button");
        actions.clickElement("login.button");
        actions.typeValueInField(generatedUser, "login.username");
        actions.typeValueInField(password, "login.password");
        actions.clickElement("login.button.submit");

        actions.waitForElementPresent("homePage.button.logout");
        actions.assertElementPresent("homePage.button.logout");

    }

}





