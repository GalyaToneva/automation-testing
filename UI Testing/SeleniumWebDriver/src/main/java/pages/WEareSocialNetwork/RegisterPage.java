package pages.WEareSocialNetwork;


import org.openqa.selenium.WebDriver;

import static com.telerikacademy.testframework.Utils.getConfigPropertyByKey;
import static pages.WEareSocialNetwork.utls.Constants.*;

public class RegisterPage extends BaseAppPage {


    public RegisterPage(WebDriver driver) {
        super(driver, "home.page");
    }


    public void registerUser() {

        navigateToPage();
        assertNavigatedUrl();
        generateRandomEmail();
        generateRandomUserName();

        String password = getConfigPropertyByKey("testUser1.password");

        actions.waitForElementClickable("homePage.button.register");
        actions.clickElement("homePage.button.register");

        actions.waitForElementPresent("registerPage.field.username");
        actions.typeValueInField(generatedUser, "registerPage.field.username");
        actions.assertElementAttribute("registerPage.field.username", "value", generatedUser);

        actions.typeValueInField(generatedEmail, "registerPage.field.email");
        actions.assertElementAttribute("registerPage.field.email", "value", generatedEmail);

        actions.typeValueInField(password, "registerPage.field.password");
        actions.typeValueInField(password, "registerPage.field.confirmPassword");
        actions.assertElementAttribute("registerPage.field.confirmPassword", "value", password);

        actions.clickElement("registerPage.button.register");

        System.out.printf("User with Username: %s , email:%s and password:%s was created",
                generatedUser, generatedEmail, password);

        actions.waitForElementClickable("registerPage.button.updateYourProfile");
        actions.assertElementPresent("registerPage.button.updateYourProfile");

    }

    public void registerUserWithValueGreaterThanMax() {
        navigateToPage();
        assertNavigatedUrl();

        generateRandomEmail();
        generateRandomInvalidUsernameValueGraterThenMax();

        String password = getConfigPropertyByKey("testUser1.password");

        actions.waitForElementClickable("homePage.button.register");
        actions.clickElement("homePage.button.register");

        actions.waitForElementPresent("registerPage.field.username");
        actions.typeValueInField(generatedInvalidUser, "registerPage.field.username");
        actions.assertElementAttribute("registerPage.field.username", "value", generatedInvalidUser);

        actions.typeValueInField(generatedEmail, "registerPage.field.email");
        actions.assertElementAttribute("registerPage.field.email", "value", generatedEmail);

        actions.typeValueInField(password, "registerPage.field.password");
        actions.typeValueInField(password, "registerPage.field.confirmPassword");
        actions.assertElementAttribute("registerPage.field.confirmPassword", "value", password);

        actions.clickElement("registerPage.button.register");

        actions.waitForElementClickable("registerPage.button.updateYourProfile");
        actions.assertElementPresent("registerPage.button.updateYourProfile");



    }

    public void registerUserWithInvalidUsernameWithDigit() {
        navigateToPage();
        assertNavigatedUrl();

        generateRandomEmail();
        generateRandomUsernameWithDigit();

        String password = getConfigPropertyByKey("testUser1.password");

        actions.waitForElementClickable("homePage.button.register");
        actions.clickElement("homePage.button.register");

        actions.waitForElementPresent("registerPage.field.username");
        actions.typeValueInField(generatedUsernameWithDidgit, "registerPage.field.username");
        actions.assertElementAttribute("registerPage.field.username", "value", generatedUsernameWithDidgit);

        actions.typeValueInField(generatedEmail, "registerPage.field.email");
        actions.assertElementAttribute("registerPage.field.email", "value", generatedEmail);

        actions.typeValueInField(password, "registerPage.field.password");
        actions.typeValueInField(password, "registerPage.field.confirmPassword");
        actions.assertElementAttribute("registerPage.field.confirmPassword", "value", password);

        actions.clickElement("registerPage.button.register");

        actions.waitForElementClickable("registerPage.button.updateYourProfile");
        actions.assertElementPresent("registerPage.button.updateYourProfile");
        System.out.printf(ANSI_GREEN + "User was created" + ANSI_RESET);
    /

    }

    public void registerUser_RandomStringEmail() {
        navigateToPage();
        assertNavigatedUrl();
        generateRandomStringAsEmail();
        actions.waitForElementClickable("homePage.button.register");
        actions.clickElement("homePage.button.register");

        actions.waitForElementPresent("registerPage.field.username");
        actions.typeValueInField(USERNAME, "registerPage.field.username");
        actions.assertElementAttribute("registerPage.field.username", "value", USERNAME);


        actions.typeValueInField(generatedRandomStringAsEmail, "registerPage.field.email");
        actions.assertElementAttribute("registerPage.field.email", "value", generatedRandomStringAsEmail);

        actions.typeValueInField(PASSWORD, "registerPage.field.password");
        actions.typeValueInField(PASSWORD, "registerPage.field.confirmPassword");
        actions.assertElementAttribute("registerPage.field.confirmPassword", "value", PASSWORD);

        actions.clickElement("registerPage.button.register");

        actions.waitForElementVisible("registerPage.alert.invalidEmail");
        actions.waitForElementPresent("registerPage.alert.invalidEmail");
        System.out.printf(ANSI_GREEN + "User is not created\n" + ANSI_RESET);


    }

    public void registerUserNotConfirmedPassword() {
        navigateToPage();
        assertNavigatedUrl();
        generateRandomEmail();

        String password = getConfigPropertyByKey("password");

        actions.waitForElementClickable("homePage.button.register");
        actions.clickElement("homePage.button.register");

        actions.waitForElementPresent("registerPage.field.username");
        actions.typeValueInField(USERNAME, "registerPage.field.username");
        actions.assertElementAttribute("registerPage.field.username", "value", USERNAME);

        actions.typeValueInField(generatedEmail, "registerPage.field.email");
        actions.assertElementAttribute("registerPage.field.email", "value", generatedEmail);

        actions.typeValueInField(PASSWORD, "registerPage.field.password");
        actions.typeValueInField(password, "registerPage.field.confirmPassword");
        actions.clickElement("registerPage.button.register");
        actions.assertElementPresent("registerPage.alert.invalidPassword");
        System.out.printf(ANSI_GREEN +"User is not created.\n Password: %s \n Confirm Password: %s \n"
                + ANSI_RESET,PASSWORD,password);
    }

}
