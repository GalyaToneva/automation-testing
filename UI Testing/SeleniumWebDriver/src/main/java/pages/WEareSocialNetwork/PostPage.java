package pages.WEareSocialNetwork;


import org.openqa.selenium.WebDriver;

import static pages.WEareSocialNetwork.utls.Constants.ANSI_GREEN;
import static pages.WEareSocialNetwork.utls.Constants.ANSI_RESET;

public class PostPage extends BaseAppPage {

    private String latestPosts = "http://localhost:8081/posts";

    public PostPage(WebDriver driver) {
        super(driver, "post.page");
    }


    public void likePost() {
        navigateToPage();
        actions.waitForElementClickable("postPage.button.newPost");
        actions.clickElement("homePage.button.latestPost");
        actions.waitForElementPresent("postPage.list.latestPost");
        actions.clickElement("postPage.allPosts.likeLatestPost");

        actions.assertElementPresent("postPage.allPosts.likeLatestPost");


    }

    public void commentPost() {

        navigateToPage();
        generateRandomComment();
        actions.waitForElementVisible("homePage.button.latestPost");
        actions.clickElement("homePage.button.latestPost");

        actions.waitForElementPresent("postPage.button.newPost");
        actions.assertElementPresent("postPage.button.exploreLatestPost");

        actions.clickElement("postPage.button.exploreLatestPost");

        actions.waitForElementClickable("postPage.description.commentField");
        actions.typeValueInField(generatedComment, "postPage.description.commentField");
        actions.clickElement("postPage.button.postComment");


        actions.waitForElementClickable("postPage.button.showComments");
        actions.assertElementPresent("postPage.button.showComments");
        actions.clickElement("postPage.button.showComments");

        actions.assertElementAttribute("postPage.description.latestCommentText", "comment-body", generatedComment);

        System.out.printf(ANSI_GREEN +"Comment '%s' was published!" + ANSI_RESET, generatedComment);

    }

    public void likeComment() {

        navigateToPage();

        actions.waitForElementClickable("homePage.button.latestPost");
        actions.clickElement("homePage.button.latestPost");

        actions.waitForElementClickable("postPage.list.latestPost");
        actions.clickElement("postPage.button.exploreLatestPost");

        actions.waitForElementClickable("postPage.button.showComments");
        actions.clickElement("postPage.button.showComments");

        actions.waitForElementClickable("postPage.button.likeLatestComment");
        actions.clickElement("postPage.button.likeLatestComment");

        actions.waitForElementVisible("postPage.button.unlikeButton");
        actions.assertElementPresent("postPage.button.unlikeButton");
        System.out.printf(ANSI_GREEN +"Comment is liked\n" + ANSI_RESET);
    }
    public void unlikeComment(){
        navigateToPage();

        actions.waitForElementClickable("homePage.button.latestPost");
        actions.clickElement("homePage.button.latestPost");

        actions.waitForElementClickable("postPage.list.latestPost");
        actions.clickElement("postPage.button.exploreLatestPost");

        actions.waitForElementClickable("postPage.button.showComments");
        actions.clickElement("postPage.button.showComments");

        actions.waitForElementClickable("postPage.button.unlikeButton");
        actions.clickElement("postPage.button.unlikeButton");


        actions.waitForElementClickable("postPage.button.likeLatestComment");
        actions.assertElementPresent("postPage.button.likeLatestComment");
        System.out.printf(ANSI_GREEN +"Comment is unliked\n" + ANSI_RESET);

    }

}
